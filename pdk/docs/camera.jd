page.title=Camera Subsystem
@jd:body

<div id="qv-wrapper">
<div id="qv">
<a name="toc"/>

<h2>In this document</h2>

<ol>
<li><a href="#androidCameraIntroduction">Introduction</a></li>
<li><a href="#androidCameraBuildingDriver">Building a Camera Library</a></li>
<li><a href="#androidCameraSequenceDiagrams">Sequence Diagrams</a>
	<ol>
		<li><a href="#androidCameraSequenceDiagramsPreview">Preview</a></li>
		<li><a href="#androidCameraSequenceDiagramsTakePic">Taking a Picture</a></li>
	</ol>
</li>
<li><a href="#androidCameraInterfaceIntro">Interface</a></li>
</ol>

</div>
</div>

<a name="androidCameraIntroduction"></a><h2>Introduction</h2>

<p>Android's camera subsystem connects the camera application to the application framework and user space libraries, which in turn communicate with the camera hardware layer that operates the physical camera.</p>
<p>The diagram below illustrates the structure of the camera subsystem.</p>
<p><img src="androidCameraArchitecture.gif"></p>


<a name="androidCameraBuildingDriver"></a><h2>Building a Camera Library</h2>

<p>To implement a camera driver, create a shared library that implements the interface defined in <code>CameraHardwareInterface.h</code>. You must name your shared library <code>libcamera.so</code> so that it will get loaded from <code>/system/lib</code> at runtime.  Place libcamera sources and <code>Android.mk</code> in <code>partner/acme/chipset_or_board/libcamera/</code>.</p>
<p>The following stub <code>Android.mk</code> file ensures that <code>libcamera</code> compiles and links to the appropriate libraries:</p>
<pre class="prettify">
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libcamera

LOCAL_SHARED_LIBRARIES := \
    libutils \
    librpc \
    liblog

LOCAL_SRC_FILES += MyCameraHardware.cpp

LOCAL_CFLAGS +=

LOCAL_C_INCLUDES +=

LOCAL_STATIC_LIBRARIES += \
    libcamera-common \
    libclock-rpc \
    libcommondefs-rpc

include $(BUILD_SHARED_LIBRARY)
</pre>


<a name="androidCameraSequenceDiagrams"></a><h2>Sequence Diagrams</h2>



<a name="androidCameraSequenceDiagramsPreview"></a><h3>Preview</h3>

<p>The following diagram illustrates the sequence of function calls and actions necessary for your camera to preview.</p>
<img src="cameraPreview.jpg">


<a name="androidCameraSequenceDiagramsTakePic"></a><h3>Taking a Picture</h3>

<p>The following diagram illustrates the sequence of function calls and actions necessary for your camera to take a picture.</p>
<img src="cameraTakePicture.jpg">


<a name="androidCameraInterfaceIntro"></a><h2>Interface</h2>



<p class="note"><strong>Note</strong>: This document relies on some Doxygen-generated content that appears in an iFrame below. To return to the Doxygen default content for this page, <a href="camera.html">click here</a>.</p>


<iframe onLoad="resizeDoxFrameHeight();" src="CameraHardwareInterface_8h.html " scrolling="no" scroll="no" id="doxygen" marginwidth="0" marginheight="0" frameborder="0" style="width:100%;"></iframe>


